import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import sort from '@/components/sort'
import order from '@/components/order'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/sort',
      name: 'sort',
      component: sort
    },
    {
      path: '/order',
      name: 'order',
      component: order
    }
  ]
})
